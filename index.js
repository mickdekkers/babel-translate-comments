const _ = require('lodash')
const babel = require('babel-core')
const DataLoader = require('dataloader')
const pMap = require('p-map')
const pProps = require('p-props')
const translate = require('google-translate-api')
const uuidv4 = require('uuid/v4')

const pickNodeProps = (cache, COMMENT_KEYS) => ({ node }) => {
  if (node) {
    const res = _.pick(node, COMMENT_KEYS)
    cache.push(res)
  }
}

const gatherPlugin = cache => ({ types: t }) => {
  return {
    name: 'xlate-comments-gather',
    visitor: {
      Program(path) {
        const pnp = pickNodeProps(cache, t.COMMENT_KEYS)
        // Process Program node
        pnp(path)

        // Process all other nodes
        path.traverse({
          enter: pnp
        })
      }
    }
  }
}

const mergeNode = (cache, indexHolder) => ({ node }) => {
  if (node) {
    Object.assign(node, cache[indexHolder.index++])
  }
}

const replacePlugin = cache => ({ types: t }) => {
  return {
    name: 'xlate-comments-replace',
    pre(state) {
      this.index = 0
    },
    visitor: {
      Program(path) {
        const rnp = mergeNode(cache, this)
        // Process Program node
        rnp(path)

        // Process all other nodes
        path.traverse({
          enter: rnp
        })
      }
    }
  }
}

// TODO: use less hacky way to pass this to translateComment (maybe Readers?)
let translationLoader

const commentPaddingRgx = /^(\s*\**\s*)(.*)/

// TODO: refactor this
// Replace a Comment's text with a translated version of that text
const translateComment = async comment => {
  // Split leading padding (including whitespace and asterisks)
  // from comment text
  const { leadingPadding, commentText } = comment.value.split('\n').reduce((
    acc,
    text
  ) => {
    const [, lp = '', ct = ''] = text.split(commentPaddingRgx)
    acc.leadingPadding.push(lp)
    acc.commentText.push(ct)
    return acc
  },
  { leadingPadding: [], commentText: [] })

  // Return comment text without empty lines
  // and keep track of which lines are empty
  const { commentTextWo, emptyLineMap } = commentText.reduce(
    (acc, ct) => {
      if (ct === '') {
        acc.emptyLineMap.push(true)
      } else {
        acc.emptyLineMap.push(false)
        acc.commentTextWo.push(ct)
      }
      return acc
    },
    { emptyLineMap: [], commentTextWo: [] }
  )

  // Return the comment unmodified if it doesn't contain any text
  if (emptyLineMap.every(x => x)) {
    return comment
  }

  // Fetch translated text
  let translation = await translationLoader.load(commentTextWo.join('\n'))

  // Reapply leading padding
  let _translation = translation.split('\n').reverse()
  translation = emptyLineMap
    .map(
      (x, i) => (x ? leadingPadding[i] : leadingPadding[i] + _translation.pop())
    )
    .join('\n')

  // Return the comment with its text translated
  return Object.assign(comment, {
    value: translation
  })
}

// Note: this assumes that `node` only contains comment properties
const translateNode = node =>
  pProps(
    _.mapValues(
      node,
      // TODO: treat adjacent comments as single comment for better translations
      comments =>
        Array.isArray(comments) ? pMap(comments, translateComment) : comments
    )
  )

const main = async (input, options) => {
  options = Object.assign({ to: 'en' }, options)
  let cache = []

  // Gather comments
  babel.transform(input, {
    plugins: [gatherPlugin(cache)],
    sourceMaps: false
  })

  translationLoader = new DataLoader(async sourceTexts => {
    // TODO: come up with a less fragile way to batch translation requests
    const separator = `\n\n\n====== ${uuidv4()} ======\n\n\n`
    const payload = sourceTexts.join(separator)

    console.log('Making translation request')
    const response = await translate(payload, options)

    return response.text.split(separator)
  })

  // Get translations
  cache = await pMap(cache, translateNode)

  // Replace comments
  const { code } = babel.transform(input, {
    plugins: [replacePlugin(cache)],
    sourceMaps: false
  })

  return code
}

main(`// Omelette du fromage`, { from: 'fr', to: 'en' }).then(console.log)
